﻿Shader "Unlit/Low-Poly-WaveShader"
{
	Properties
	{
		_Color("Color", color) = (1,1,1,0)
		_SpecColor("Spec color (reflection color)", color) = (0.5,0.5,0.5,0.5)

		//Custom wave effects
		_Shininess("Shininess", Range(0.1, 10.0)) = 0.3
		_WaveLength("Wave length", Range(0.0, 2.0)) = 0.3
		_WaveHeight("Wave height", Range(0.0, 1.0)) = 0.5
		_WaveSpeed("Wave speed", Range(0.0, 10.0)) = 0.75



		_RandomSpeed("Random Speed", Range(0.0, 25.0)) = 0.3
		_RandomHeight("Random height", Range(0.0, 10.0)) = 0.1


		//extended features
		_LightSmoothness("Light Smoothness", Range(0.0,3.0)) = 3

		//how much light is kept or received from the source light
		_Attenuation("Attenuation", Range(0,2.0)) = 1

		//Randomises the height of the vertexes, does not work with the wave
			
	}


		SubShader
	{
		Tags{ "Queue" = "Geometry" "RenderType" = "Opaque" }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
	{
		Tags{ "LightMode" = "ForwardBase" }

		CGPROGRAM


#pragma vertex vert
#pragma geometry geom
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest

#pragma multi_compile_fwdbase
#pragma multi_compile_fog
#include "UnityCG.cginc"
#include "HLSLSupport.cginc"
#include "UnityStandardCore.cginc"

		//the two points of randomisation used in the wave, you can change these to whatever you like to make the wave suit your tastes better.
		float rand(float3 co)
	{
		return frac(sin(dot(co.xyz ,float3(13.0,80.0,46.0))) * 43.0);
	}

	float rand2(float3 co)
	{
		return frac(sin(dot(co.xyz ,float3(20.0,75.0,35.0))) * 15.0);

	}


	float _WaveLength;
	float _WaveHeight;
	float _WaveSpeed;
	float _RandomHeight;
	float _RandomSpeed;

	uniform float _Shininess;

	//light smoothing on the tris
	float _LightSmoothness;

	//how much light is acctually received
	float _Attenuation;

	struct v2g
	{
		float4 pos: SV_POSITION;

		//Position in world
		float4 posWorld : TEXCOORD1;
		float3 diffuseColor : TEXCOORD9;
		float3 specularColor : TEXCOORD10;
	};

	v2g vert(VertexInput v)
	{
		//o means output and can be any decleration you choose.
		v2g o;
		UNITY_INITIALIZE_OUTPUT(v2g, o);

		//v0 is the vertices within the object it is modifying
		float3 v0 = mul((float3x3)unity_ObjectToWorld, v.vertex).xyz;

		float phase0 = (_WaveHeight)* sin((_Time[1] * _WaveSpeed) + (v0.x * _WaveLength) + (v0.z * _WaveLength) + rand2(v0.xzz));
		float phase1 = (_RandomHeight)*sin(cos(rand(v0.xzz) * _RandomHeight * cos(_Time[1] * _RandomSpeed * sin(rand(v0.xxz)))));

		//using the two phases above you are given a sine wave effect.
		v0.y += phase0 + phase1; //Assignes the wave to the y axis of the vertexes
	
								 //finds the world position
		v.vertex.xyz = mul((float3x3)unity_WorldToObject, v0);
		float4 posWorld = mul(unity_ObjectToWorld, v.vertex);

		//outputs the world position
		o.posWorld = posWorld;

		//outputs the vertex positions as pos
		o.pos = v.vertex;

		//finds the vertexes normals and sets them as normalWorld in this shader
		float3 normalWorld = UnityObjectToWorldNormal(v.normal);

		//closing decleration, used to state that this section is finished
		return o;

	}

	//editing the geometry/ visual outputs
	//geom short for geometry
	[maxvertexcount(3)]
	void geom(triangle v2g  IN[3], inout TriangleStream<v2g> triStream)
	{

		//tristream is treated in a similar fashion to the o (output) as before sort of like an array or list in C#
		//this section effects the way color and light is rendered

		//setting up the output again, this isnt used till a little bit latter and is just personal preference to have it here
		v2g o;
		UNITY_INITIALIZE_OUTPUT(v2g, o);

		//three vertex positions of a tri
		float3 v0 = IN[0].pos.xyz;
		float3 v1 = IN[1].pos.xyz;
		float3 v2 = IN[2].pos.xyz;

		//used to find the center of the tri, and create color smoothing with the lighting.
		//In this case it is publiclly editable how smooth the lighting is.
		float3 centerPos = (v0 + v1 + v2) / _LightSmoothness;

		//computing the vertex normals
		float3 vertexNormals = normalize(cross(v1 - v0, v2 - v0));

		//model matrix and model matrix inverse is used to find the rendering angles and directions
		float4x4 modelMatrix = unity_ObjectToWorld;
		float4x4 modelMatrixInverse = unity_WorldToObject;

		//used to find the normal direction and its rendered angle
		float3 normalDirection = normalize(mul(float4(vertexNormals, 0.0), modelMatrixInverse).xyz);

		//finding the camera angle/direction for rendering purposes
		float3 viewDirection = normalize(_WorldSpaceCameraPos - mul(modelMatrix, float4(centerPos, 0.0)).xyz);

		//finding where the light is coming from and it hits the normals of the vertexes so the light is reflected properly
		float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

		float attenuation = _Attenuation;

		//ambient light
		float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;

		//Reflection of light created by the normal direction and light direction edited by the light color
		float3 diffuseReflection = attenuation * _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

		float3 specularReflection;
		//if the normal and light direction is less than 0 then the specular reflection will also be 0, anything higher and it will reflect
		if (dot(normalDirection, lightDirection) < 0.0)
		{
			specularReflection = float3(0.0, 0.0, 0.0);
		}
		else
		{
			specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
		}

		o.pos = IN[0].pos;
		o.pos = UnityObjectToClipPos(IN[0].pos);
		o.diffuseColor = ambientLighting + diffuseReflection / 4;
		o.specularColor = specularReflection + diffuseReflection * 3 / 4;
		triStream.Append(o);

		o.pos = IN[1].pos;
		o.pos = UnityObjectToClipPos(IN[1].pos);
		o.diffuseColor = ambientLighting + diffuseReflection / 4;
		o.specularColor = specularReflection + diffuseReflection * 3 / 4;
		triStream.Append(o);

		o.pos = IN[2].pos;
		o.pos = UnityObjectToClipPos(IN[2].pos);
		o.diffuseColor = ambientLighting + diffuseReflection / 4;
		o.specularColor = specularReflection + diffuseReflection * 3 / 4;
		triStream.Append(o);

	}

	half4 frag(v2g IN) : COLOR
	{
		return float4(IN.specularColor +
		IN.diffuseColor, 1.0);
	}
		ENDCG
	}

	}
		Fallback "Diffuse"
}